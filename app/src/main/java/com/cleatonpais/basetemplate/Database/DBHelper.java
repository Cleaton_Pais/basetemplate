package com.cleatonpais.basetemplate.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.cleatonpais.basetemplate.Modules.Login.Model.UserModel;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DatabaseName = "Demo_DB.db";
    Context contextDB;
    SQLiteDatabase sqLiteDatabase;

    String CREATE_USER_MASTER = "create table USER_MASTER " +
            "(id integer primary key NOT NULL, " +
            "Username text NOT NULL, " +
            "Name text NOT NULL, " +
            "Password text NOT NULL);";

    public DBHelper(@Nullable Context context) {
        super(context, DatabaseName, null, 1);
        contextDB = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_USER_MASTER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTs CREATE_USER_MASTER");
    }

    public void insertUserData(String username, String name, String password) {
        sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Username", username);
        contentValues.put("Name", name);
        contentValues.put("Password", password);
        sqLiteDatabase.insert("USER_MASTER", null, contentValues);
    }

    public boolean checkRecords() {
        sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM USER_MASTER", null);
        return cursor.getCount() > 0;
    }

//    for spinner data
    public ArrayList<String> getEDINo() {
        ArrayList<String> edilist = new ArrayList<>();
        sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT EDI_No FROM LABEL_MASTER", null);
        cursor.moveToFirst();
        try {
            while (!cursor.isAfterLast()) {
                edilist.add(cursor.getString(cursor.getColumnIndex("EDI_No")));
                cursor.moveToNext();
            }
            cursor.close();
            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("checkError", e.getMessage());
        } finally {
            sqLiteDatabase.endTransaction();
            sqLiteDatabase.close();
        }
        if (sqLiteDatabase.isOpen())
            sqLiteDatabase.close();

        return edilist;
    }

    public UserModel getUserData(String username){
        UserModel user = new UserModel();

        sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.beginTransaction();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM USER_MASTER WHERE Username = '"+username+"'", null);
        cursor.moveToFirst();
        try {
            while (!cursor.isAfterLast()) {
                user.setUserName(cursor.getString(cursor.getColumnIndex("Username")));
                user.setName(cursor.getString(cursor.getColumnIndex("Name")));
                user.setPassword(cursor.getString(cursor.getColumnIndex("Password")));
                cursor.moveToNext();
            }
            cursor.close();
            sqLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("checkError", e.getMessage());
        } finally {
            sqLiteDatabase.endTransaction();
            sqLiteDatabase.close();
        }
        if (sqLiteDatabase.isOpen())
            sqLiteDatabase.close();
        return user;
    }
}
