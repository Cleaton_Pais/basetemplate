package com.cleatonpais.basetemplate.Modules.Main.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.cleatonpais.basetemplate.Database.DBHelper;
import com.cleatonpais.basetemplate.Modules.Login.Activity.LoginActivity;
import com.cleatonpais.basetemplate.Modules.Login.Model.UserModel;
import com.cleatonpais.basetemplate.Network.ApiClient;
import com.cleatonpais.basetemplate.R;
import com.cleatonpais.basetemplate.Utils.PreferenceUtil;
import com.cleatonpais.basetemplate.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    Context context;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        context = this;
        dbHelper = new DBHelper(context);

        if(PreferenceUtil.isUserLoggedIn()) {
            String username = PreferenceUtil.getUser().getUserName().toLowerCase();
            UserModel user = dbHelper.getUserData(username);
            String name = user.getName();
            binding.tvWelcomeUser.setText(null);
            binding.tvWelcomeUser.append("Welcome, "+name);
        } else {
            logoutUser();
        }
    }

    public void onBackPressed() {
        openAlertDialog();
    }

    public void openAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Attention.!");
        alertDialog.setMessage("Are you sure want to Logout..?");
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
//                code changes
            logoutUser();
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
    }

    public void logoutUser() {
        PreferenceUtil.setUserLoggedIn(false);
        PreferenceUtil.clearUserData();
        PreferenceUtil.setIPAddress(PreferenceUtil.getIPAddress());
        ApiClient.changeApiBaseUrl(PreferenceUtil.getIPAddress());
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
//        finish();
    }
}