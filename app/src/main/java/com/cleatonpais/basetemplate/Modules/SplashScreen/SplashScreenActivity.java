package com.cleatonpais.basetemplate.Modules.SplashScreen;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.cleatonpais.basetemplate.BuildConfig;
import com.cleatonpais.basetemplate.Database.DBHelper;
import com.cleatonpais.basetemplate.Modules.Login.Activity.LoginActivity;
import com.cleatonpais.basetemplate.Modules.Login.Model.UserModel;
import com.cleatonpais.basetemplate.Modules.Main.Activity.MainActivity;
import com.cleatonpais.basetemplate.R;
import com.cleatonpais.basetemplate.Utils.PreferenceUtil;
import com.cleatonpais.basetemplate.databinding.ActivitySplashScreenBinding;

import java.util.ArrayList;
import java.util.Arrays;


public class SplashScreenActivity extends AppCompatActivity {

    ActivitySplashScreenBinding binding;
    Context context;

    ArrayList<UserModel> listUser = new ArrayList<>();
    String tag = "Splashscreen";

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        context = this;

        binding.tvVersion.append(getString(R.string.version, BuildConfig.VERSION_NAME));

        dbHelper = new DBHelper(context);
        boolean dataExist = dbHelper.checkRecords();
        Log.e(tag, "dataExist: " + dataExist);
        if (!dataExist) loadDummyIntoDB();

        if (checkUserPermission()) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                if (!PreferenceUtil.isUserLoggedIn()) {
                    startActivity(new Intent(context, LoginActivity.class));
                } else {
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                }
                finish();
            }, 3000);
        }
    }

    public boolean checkUserPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }  //permission is automatically granted on sdk<23 upon installation

        return true;
    }


    public void loadDummyIntoDB() {
        ArrayList<String> listUsername = new ArrayList<>(Arrays.asList("admin", "markss", "root", "user"));

        ArrayList<String> listPassword = new ArrayList<>(Arrays.asList("admin", "markss", "root", "user"));

        ArrayList<String> listName = new ArrayList<>(Arrays.asList("Sirish Dixit", "Ram Paul", "Jhon White", "Sita Singh"));

        listUser = new ArrayList<>();
        for (int i = 0; i < listUsername.size(); i++) {
            UserModel userdata = new UserModel();
            userdata.setUserName(listUsername.get(i));
            userdata.setPassword(listPassword.get(i));
            userdata.setName(listName.get(i));
            listUser.add(userdata);
        }
        Log.e(tag, listUser.toString());
        insertUserDB();
    }

    public void insertUserDB() {
        for (int i = 0; i < listUser.size(); i++) {
            dbHelper.insertUserData(listUser.get(i).getUserName(), listUser.get(i).getName(), listUser.get(i).getPassword());
        }
    }

}