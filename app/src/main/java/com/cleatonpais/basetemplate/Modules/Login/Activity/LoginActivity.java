package com.cleatonpais.basetemplate.Modules.Login.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.cleatonpais.basetemplate.Modules.Login.Model.UserModel;
import com.cleatonpais.basetemplate.Modules.Main.Activity.MainActivity;
import com.cleatonpais.basetemplate.Network.ApiClient;
import com.cleatonpais.basetemplate.R;
import com.cleatonpais.basetemplate.Utils.Common;
import com.cleatonpais.basetemplate.Utils.PreferenceUtil;
import com.cleatonpais.basetemplate.databinding.ActivityLoginBinding;
import com.cleatonpais.basetemplate.databinding.DialogIpconfigBinding;
import com.cleatonpais.basetemplate.databinding.DialogValidationBinding;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLoginBinding binding;
    Context context;
    String tag = "Login Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        context = this;

        binding.btnipconfig.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
        binding.edtUsername.addTextChangedListener(new GenericTextWatcher(binding.edtUsername));
        binding.edtPassword.addTextChangedListener(new GenericTextWatcher(binding.edtPassword));

//        temp code
//        binding.edtUsername.setText("admin");
//        binding.edtPassword.setText("admin");

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btnipconfig) {
            displayValidationDialog();
        } else if (id == R.id.btnLogin) {
            validateLogin();
        }
    }

    private class GenericTextWatcher implements TextWatcher {

        private final View view;

        GenericTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            int id = view.getId();
            if (id == R.id.edtUsername) {
                if (editable.length() > 0) {
                    clearError(1);
                }
            } else if (id == R.id.edtPassword) {
                if (editable.length() > 0) {
                    clearError(2);
                }
            }
        }
    }

    DialogValidationBinding bindingValidationDialog;
    Dialog dialogValidation;

    public void displayValidationDialog() {
        bindingValidationDialog = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_validation, null, false);
        dialogValidation = new Dialog(context);
        dialogValidation.setContentView(bindingValidationDialog.getRoot());
        dialogValidation.show();
        bindingValidationDialog.tvmodules.setText("for " + getString(R.string.ip_config));

        bindingValidationDialog.btncancel.setOnClickListener(view -> dialogValidation.dismiss());

        bindingValidationDialog.btnenter.setOnClickListener(view -> {
            String userPassword = bindingValidationDialog.edtPassword.getText().toString();
            if (TextUtils.isEmpty(userPassword)) {
                bindingValidationDialog.tilPassword.setError(getString(R.string.password_error));
                bindingValidationDialog.tilPassword.setErrorIconDrawable(null);
            } else {
                if (userPassword.equals("mil")) {
                    bindingValidationDialog.tilPassword.setError(null);
                    bindingValidationDialog.tilPassword.setErrorEnabled(false);
                    dialogValidation.dismiss();
                    displayconfigIPDialog();
                } else {
                    bindingValidationDialog.tilPassword.setError(getString(R.string.password_error));
                    bindingValidationDialog.tilPassword.setErrorIconDrawable(null);
                }

            }
        });
    }

    public void displayconfigIPDialog() {
        DialogIpconfigBinding bindingIPConfigDialog = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_ipconfig, null, false);
        Dialog dialogIPConfig = new Dialog(context);
        dialogIPConfig.setContentView(bindingIPConfigDialog.getRoot());
        dialogIPConfig.show();

        bindingIPConfigDialog.edtConfigIP.append(PreferenceUtil.getIPAddress());
        bindingIPConfigDialog.tvcurrentip.setText(PreferenceUtil.getIPAddress());

        bindingIPConfigDialog.btncancel.setOnClickListener(view -> dialogIPConfig.dismiss());

        bindingIPConfigDialog.btnsave.setOnClickListener(view -> {
            if (!TextUtils.isEmpty(bindingIPConfigDialog.edtConfigIP.getText())) {
                String ipAddress = bindingIPConfigDialog.edtConfigIP.getText().toString().trim();
                PreferenceUtil.setIPAddress(ipAddress);
                ApiClient.changeApiBaseUrl(ipAddress);
                dialogIPConfig.dismiss();
                Common.showShortCustomToast(context, "Saved successfully", getResources().getColor(R.color.green));
            } else {
                Common.showShortCustomToast(context, "Enter IP Address", getResources().getColor(R.color.red));
            }
        });
    }

    public void validateLogin() {
        boolean isLoginValid = true;

        if (TextUtils.isEmpty(binding.edtUsername.getText())) {
            isLoginValid = false;
            binding.tilUsername.setError(getString(R.string.empty_field_error));
            binding.tilUsername.setErrorIconDrawable(null);
        }

        if (TextUtils.isEmpty(binding.edtPassword.getText())) {
            isLoginValid = false;
            binding.tilPassword.setError(getString(R.string.empty_field_error));
            binding.tilPassword.setErrorIconDrawable(null);
        }

        if (isLoginValid) {
            Common.showProgressDialog(context, "Please Wait.!" );
            submitLogin();
        }
    }

    public void submitLogin() {

        String userName = binding.edtUsername.getText().toString().trim();
        String password = binding.edtPassword.getText().toString().trim();

        UserModel userModel = new UserModel();
        userModel.setUserName(userName);
        userModel.setPassword(password);

        if(userName.equalsIgnoreCase("admin") && password.equalsIgnoreCase("admin")){
            PreferenceUtil.setUserLoggedIn(true);
            UserModel userLogin = userModel;
            PreferenceUtil.setUser(userLogin);
            proceedtoMain();
            Common.hideProgressDialog();
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.invalid_credentials), Toast.LENGTH_SHORT).show();
            Common.hideProgressDialog();
        }

//        Call<UserModel> call = ApiClient.getApiInterface().loginUser(userModel);
//        call.enqueue(new Callback<UserModel>() {
//            @Override
//            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
//                Log.e("resp", "" + call.request().url());
//                if (response.isSuccessful()) {
//                    if (response.body() != null && response.body().getCode().equals(1)) {
//                        UserModel userLogin = response.body();
//                        PreferenceUtil.setUser(userLogin);
//                        PreferenceUtil.setUserLoggedIn(true);
//                        proceedtoMain();
//                    } else {
//                        Common.showShortCustomToast(context, response.body().getMsg(), getResources().getColor(R.color.green));
//                    }
//                    Common.hideProgressDialog();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<UserModel> call, Throwable t) {
//                Log.e(tag, t.getMessage());
//                Toast.makeText(LoginActivity.this, "Unable to Connect. Please check IP Address", Toast.LENGTH_SHORT).show();
//                Common.hideProgressDialog();
//            }
//        });

    }

    public void proceedtoMain() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void clearError(int code) {
        switch (code) {
            case 0:
                binding.tilUsername.setError(null);
                binding.tilUsername.setErrorEnabled(false);
                binding.tilPassword.setError(null);
                binding.tilPassword.setErrorEnabled(false);
                break;
            case 1:
                binding.tilUsername.setError(null);
                binding.tilUsername.setErrorEnabled(false);
                break;
            case 2:
                binding.tilPassword.setError(null);
                binding.tilPassword.setErrorEnabled(false);
                break;
        }
    }
}