package com.cleatonpais.basetemplate.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.cleatonpais.basetemplate.R;

import java.io.IOException;

public class Common {

//    show custom toast
//    Common.showLongCustomToast(context, userLogin.getMsg(), getResources().getColor(R.color.green));
    public static void showLongCustomToast(Context context, String message, int textColor) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.curved_white);
        TextView text = view.findViewById(android.R.id.message);
        text.setPadding(10, 10, 10, 10);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setTextColor(textColor);
        toast.show();
    }

//    Common.showShortCustomToast(context, userLogin.getMsg(), getResources().getColor(R.color.red));
    public static void showShortCustomToast(Context context, String message, int textColor) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.curved_white);
        TextView text = view.findViewById(android.R.id.message);
        text.setPadding(10, 10, 10, 10);
        text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        text.setTextColor(textColor);
        toast.show();
    }

//    toogle Progress Dialog display
    static ProgressDialog progressDialog;

    public static void showProgressDialog(Context context, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();
    }

    public static void hideProgressDialog() {
        progressDialog.dismiss();
    }


//    play tone sound
    public static void playSound(int code) {
        ToneGenerator generator = new ToneGenerator(AudioManager.STREAM_ALARM, ToneGenerator.MAX_VOLUME);
//        play success tone
        if (code == 1)
        {
            generator.startTone(ToneGenerator.TONE_CDMA_REORDER);
            SystemClock.sleep(500);
            generator.release();
        } else if (code == 0) {
//            play error tone
            generator.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD);
            SystemClock.sleep(700);
            generator.release();
        }
    }

//    play audio from file
//        Common.startSound(context, "mp3/op_complete.mp3");
    public static void startSound(Context context, String filename) {
        AssetFileDescriptor afd = null;
        try {
            afd = context.getResources().getAssets().openFd(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        MediaPlayer player = new MediaPlayer();
        try {
            assert afd != null;
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            player.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        player.start();
    }

//    add text view for table or grid
    public static TextView getTextView(Context context, String text, int textColor) {
        TextView tv = new TextView(context);
        tv.setBackgroundResource(R.drawable.red_cell_shape);
        tv.setTextColor(ContextCompat.getColor(context, textColor));
        tv.setText(text);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, context.getResources().getDisplayMetrics());
        tv.setPaddingRelative(padding, padding, padding, padding);

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1; //this is mandatory to get equal width between all columns in TableRow
        tv.setLayoutParams(layoutParams);

        return tv;
    }


}
